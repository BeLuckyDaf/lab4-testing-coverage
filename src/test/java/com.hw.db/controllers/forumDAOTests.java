package com.hw.db.controllers;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;
import org.junit.jupiter.api.DisplayName;
import org.testng.annotations.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class forumDAOTests {
    @Test
    @DisplayName("List of users with no agruments")
    void UserListTest1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        UserDAO.UserMapper USER_MAPPER = new UserDAO.UserMapper();
        ForumDAO.UserList("slug", null, null, null);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;"), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("List of users since 1000")
    void UserListTest2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        UserDAO.UserMapper USER_MAPPER = new UserDAO.UserMapper();
        ForumDAO.UserList("slug", null, "1000", null);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND nickname > (?)::citext ORDER BY nickname;"), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("List of users since 1000 desc")
    void UserListTest3() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        UserDAO.UserMapper USER_MAPPER = new UserDAO.UserMapper();
        ForumDAO.UserList("slug", null, "1000", true);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND nickname < (?)::citext ORDER BY nickname desc;"), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("List of users limit 10")
    void UserListTest4() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        UserDAO.UserMapper USER_MAPPER = new UserDAO.UserMapper();
        ForumDAO.UserList("slug", 10, null, null);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname LIMIT ?;"), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("List of users since 1000 desc limit 10")
    void UserListTest5() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        UserDAO.UserMapper USER_MAPPER = new UserDAO.UserMapper();
        ForumDAO.UserList("slug", 10, "1000", true);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("List of users desc")
    void UserListTest6() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        UserDAO.UserMapper USER_MAPPER = new UserDAO.UserMapper();
        ForumDAO.UserList("slug", null, null, true);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc;"), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("List of users since 1000 limit 10")
    void UserListTest7() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        UserDAO.UserMapper USER_MAPPER = new UserDAO.UserMapper();
        ForumDAO.UserList("slug", 10, "1000", null);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND nickname > (?)::citext ORDER BY nickname LIMIT ?;"), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("List of users limit 10 desc")
    void UserListTest8() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        UserDAO.UserMapper USER_MAPPER = new UserDAO.UserMapper();
        ForumDAO.UserList("slug", 10, null, true);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc LIMIT ?;"), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }
}
