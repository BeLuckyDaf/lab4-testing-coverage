package com.hw.db.controllers;

import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;
import org.junit.jupiter.api.DisplayName;
import org.testng.annotations.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class threadDAOTests {
    @Test
    @DisplayName("TreeSort with desc true")
    void TreeSortTest1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ThreadDAO threadDAO = new ThreadDAO(mockJdbc);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        ThreadDAO.ThreadMapper THREAD_MAPPER = new ThreadDAO.ThreadMapper();
        ThreadDAO.treeSort(1, 1, 1, false);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;"), Mockito.any(PostDAO.PostMapper.class), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("TreeSort with desc false")
    void TreeSortTest2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ThreadDAO threadDAO = new ThreadDAO(mockJdbc);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        ThreadDAO.ThreadMapper THREAD_MAPPER = new ThreadDAO.ThreadMapper();
        ThreadDAO.treeSort(1, 1, 1, true);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"), Mockito.any(PostDAO.PostMapper.class), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }
}
