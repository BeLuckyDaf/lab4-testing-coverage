package com.hw.db.controllers;

import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;
import org.junit.jupiter.api.DisplayName;
import org.testng.annotations.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.*;

class userDAOTests {
    @Test
    @DisplayName("Change with all fields")
    void ChangeTest1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("hsabol", "abol@kiev.su", "Hilbert", "Straus");
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("Change with email")
    void ChangeTest2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("hsabol", "abol@kiev.su", null, null);
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("Change with email and name")
    void ChangeTest3() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("hsabol", "abol@kiev.su", "Hilbert", null);
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("Change with email and about")
    void ChangeTest4() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("hsabol", "abol@kiev.su", null, "Straus");
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("Change with fullname and about")
    void ChangeTest5() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("hsabol", null, "Hilbert", "Straus");
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("Change with fullname and about")
    void ChangeTest6() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("hsabol", null, "Hilbert", null);
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("Change with about")
    void ChangeTest7() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("hsabol", null, null, "Straus");
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("Change with nothing")
    void ChangeTest8() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("hsabol", null, null, null);
        UserDAO.Change(user);
        verifyNoInteractions(mockJdbc);
    }
}
