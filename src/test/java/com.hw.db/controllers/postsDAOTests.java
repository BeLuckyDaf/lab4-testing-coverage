package com.hw.db.controllers;

import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;
import org.junit.jupiter.api.DisplayName;
import org.testng.annotations.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;

import static org.mockito.Mockito.*;

class postsDAOTests {
    @Test
    @DisplayName("SetPost with AUTHOR")
    void SetPostTest1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Post oldPost = new Post("Jenny", new Timestamp(0), "forum", "Don't panic!", 1, 1, true);
        Post post = new Post("JennyNEW", new Timestamp(0), "forum", "Don't panic!", 1, 1, true);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(oldPost);
        PostDAO.setPost(1, post);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("SetPost with AUTHOR AND MESSAGE")
    void SetPostTest2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Post oldPost = new Post("Jenny", new Timestamp(0), "forum", "Don't panic!", 1, 1, true);
        Post post = new Post("JennyNEW", new Timestamp(0), "forum", "Don't panic!NEW", 1, 1, true);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(oldPost);
        PostDAO.setPost(1, post);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("SetPost with AUTHOR AND MESSAGE AND TIME")
    void SetPostTest3() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Post oldPost = new Post("Jenny", new Timestamp(0), "forum", "Don't panic!", 1, 1, true);
        Post post = new Post("JennyNEW", new Timestamp(1), "forum", "Don't panic!NEW", 1, 1, true);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(oldPost);
        PostDAO.setPost(1, post);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("SetPost with MESSAGE AND TIME")
    void SetPostTest4() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Post oldPost = new Post("Jenny", new Timestamp(0), "forum", "Don't panic!", 1, 1, true);
        Post post = new Post("Jenny", new Timestamp(1), "forum", "Don't panic!NEW", 1, 1, true);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(oldPost);
        PostDAO.setPost(1, post);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("SetPost with MESSAGE")
    void SetPostTest5() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Post oldPost = new Post("Jenny", new Timestamp(0), "forum", "Don't panic!", 1, 1, true);
        Post post = new Post("Jenny", new Timestamp(0), "forum", "Don't panic!NEW", 1, 1, true);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(oldPost);
        PostDAO.setPost(1, post);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("SetPost with TIME")
    void SetPostTest6() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Post oldPost = new Post("Jenny", new Timestamp(0), "forum", "Don't panic!", 1, 1, true);
        Post post = new Post("Jenny", new Timestamp(1), "forum", "Don't panic!", 1, 1, true);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(oldPost);
        PostDAO.setPost(1, post);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("SetPost with AUTHOR AND TIME")
    void SetPostTest7() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Post oldPost = new Post("Jenny", new Timestamp(0), "forum", "Don't panic!", 1, 1, true);
        Post post = new Post("JennyNEW", new Timestamp(1), "forum", "Don't panic!", 1, 1, true);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(oldPost);
        PostDAO.setPost(1, post);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("SetPost with nothing")
    void SetPostTest8() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Post oldPost = new Post("Jenny", new Timestamp(0), "forum", "Don't panic!", 1, 1, true);
        Post post = new Post("Jenny", new Timestamp(0), "forum", "Don't panic!", 1, 1, true);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(oldPost);
        PostDAO.setPost(1, post);
        verify(mockJdbc).queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1));
        verifyNoMoreInteractions(mockJdbc);
    }
}
